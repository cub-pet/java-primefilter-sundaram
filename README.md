This is simple project to experiment with multi-threading.

The task was: to find N first prime numbers.

To make it work in several threads I was need to solve task "find all primes between N and M".

The style of code is really ugly, because the only goal was to learn multi-threading.
