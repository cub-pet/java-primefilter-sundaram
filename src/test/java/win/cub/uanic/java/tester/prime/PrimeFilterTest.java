package win.cub.uanic.java.tester.prime;

import java.util.List;

import static org.junit.Assert.assertArrayEquals;

public class PrimeFilterTest {
    void filterPrimesTest(PrimeFilter primeFilter) {
        filterPrimesSingleTest(
                primeFilter,
                1, 2,
                new long[]{2L}
        );

        filterPrimesSingleTest(
                primeFilter,
                1, 10,
                new long[]{2L, 3L, 5L, 7L}
        );

        filterPrimesSingleTest(
                primeFilter,
                2, 9,
                new long[]{2L, 3L, 5L, 7L}
        );

        filterPrimesSingleTest(
                primeFilter,
                20, 30,
                new long[]{23L, 29L}
        );

        filterPrimesSingleTest(
                primeFilter,
                20, 22,
                new long[]{}
        );

        filterPrimesSingleTest(
                primeFilter,
                50, 100,
                new long[]{
                        53L,
                        59L, 61L, 67L, 71L, 73L,
                        79L, 83L, 89L, 97L
                }
        );

        filterPrimesSingleTest(
                primeFilter,
                1, 100,
                new long[]{
                        2L, 3L, 5L, 7L, 11L, 13L,
                        17L, 19L, 23L, 29L, 31L,
                        37L, 41L, 43L, 47L, 53L,
                        59L, 61L, 67L, 71L, 73L,
                        79L, 83L, 89L, 97L
                }
        );

        filterPrimesSingleTest(
                primeFilter,
                90, 100,
                new long[]{97L}
        );
    }

    void filterPrimesSingleTest(
            PrimeFilter primeFilter,
            int from, int to,
            long[] expected
    ) {
        final long[] primes = primeFilter.filterPrimes(from, to);
        assertArrayEquals(expected, primes);
    }
}
