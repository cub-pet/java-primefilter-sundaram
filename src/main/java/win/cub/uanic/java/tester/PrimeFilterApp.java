package win.cub.uanic.java.tester;

import win.cub.uanic.java.tester.prime.SundaramPrimeFilter;

public class PrimeFilterApp {

    public static void main(String[] args) {
        final SundaramPrimeFilter sundaramPrimeFilter = new SundaramPrimeFilter();
        long[] result = sundaramPrimeFilter.filterPrimes(1, 15485857);
        System.out.println(result);
    }

}
