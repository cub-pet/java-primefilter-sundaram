package win.cub.uanic.java.tester.prime;

import java.util.List;

public interface PrimeFilter {
    long[] filterPrimes(int from, int to);
}
