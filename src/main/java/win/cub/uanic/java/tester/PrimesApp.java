package win.cub.uanic.java.tester;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;

import static java.util.Collections.min;

public class PrimesApp {
    static int concurrency;
    static ThreadPoolExecutor executor;
    static Map<Integer,Future<FilterPrimesResult>> resultList;

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        int from = 1;
        int neededPrimes = 100_000_000;

        int interval = 500_000;
        int printedPrimes = 0;

        concurrency = Runtime.getRuntime().availableProcessors();
        System.out.println("concurrency: " + concurrency + "\n");
        printDateTime();

        executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(concurrency);
        resultList = new HashMap<>();

        int i = 0;
        while (i < concurrency) {
            int to = from + interval - 1;
            createFilterPrimesThread(i, from, to);
            from += interval;
            i++;
        }

        while (printedPrimes < neededPrimes) {
            int to = from + interval - 1;
            createFilterPrimesThread(i, from, to);

            final Integer minIndex = min(resultList.keySet());
            Future<FilterPrimesResult> threadResult = resultList.remove(minIndex);
            FilterPrimesResult result = threadResult.get();

            int primesToPrint = Math.min(result.count, neededPrimes - printedPrimes);
//            StringBuilder builder = new StringBuilder();
//            for (int idx = 0; idx < primesToPrint; idx++) {
//                builder.append(result.primes[idx]);
//                builder.append(", ");
//            }
//            System.out.print(builder.toString());
            printedPrimes += primesToPrint;

            from += interval;
            i++;
        }

        executor.shutdown();

        printDateTime();
        System.out.println("\n");
    }

    private static void createFilterPrimesThread(int i, int from, int to) {
        final FilterPrimesCallable filterPrimesCallable = new FilterPrimesCallable(from, to);
        final Future<FilterPrimesResult> filterPrimesResultFuture = executor.submit(filterPrimesCallable);
        resultList.put(i, filterPrimesResultFuture);
    }

    private static void printDateTime() {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        System.out.println(dtf.format(now));
    }

}
