package win.cub.uanic.java.tester;

import java.util.Arrays;
import java.util.concurrent.Callable;

public class FilterPrimesCallable implements Callable<FilterPrimesResult> {
    int from;
    int to;

    public FilterPrimesCallable(int from, int to) {
        this.from = from;
        this.to = to;
    }

    @Override
    public FilterPrimesResult call() {
        long[] result = filterPrimes(from, to);

        return new FilterPrimesResult(result.length, result);
    }

    long[] filterPrimes(int from, int to) {
        assert (from >= 1);
        assert (to > from);

        int minCandidate = from;
        if (minCandidate < 3) minCandidate = 3;
        minCandidate = minCandidate / 2;
        int maxCandidate = (to - 1) / 2;

        byte[] candidateIndex = new byte[maxCandidate - minCandidate + 1];
        Arrays.fill(candidateIndex, (byte) 1);

        for (int i = 1; 2 * i * (i + 1) <= maxCandidate; i++) {
            int j = (int) Math.max(i, Math.ceil((double) (minCandidate - i) / (2 * i + 1)));
            int idx = 2 * i * j + i + j;
            while (idx <= maxCandidate) {
                candidateIndex[idx - minCandidate] = 0;
                j++;
                idx = 2 * i * j + i + j;
            }
        }

        int resultIdx = 0;
        final long[] result = new long[maxCandidate - minCandidate + 2];
        if (from < 3) {
            result[resultIdx++] = 2;
        }
        for (int idx = 0; idx < candidateIndex.length; idx++) {
            if (candidateIndex[idx] == 1) {
                result[resultIdx++] = 2 * (idx + minCandidate) + 1;
            }
        }

        return Arrays.copyOf(result, resultIdx);
    }
}
