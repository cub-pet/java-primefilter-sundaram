package win.cub.uanic.java.tester;

public class FilterPrimesResult {
    public int count;
    public long[] primes;

    public FilterPrimesResult(int count, long[] primes) {
        this.count = count;
        this.primes = primes;
    }
}
